// This file is auto generated
#pragma once

#include <string>

#ifndef COLOR_TYPE
	typedef unsigned int color;
	#define COLOR_TYPE
#endif

#ifndef PI
	#define PI 3.14159265358979323846f
#endif
#ifndef E
	#define E 2.71828182845904523536f
#endif

namespace Textures
{
	namespace Filepaths
	{
		const std::string DOG = "Assets/Dog.png";
		const color CAT = 0xFF81A2f2;
	} // namespace Filepaths

} // namespace Textures

namespace Globals
{
	namespace Vars
	{
		const int TARGET_FPS = 60;
	} // namespace Vars

} // namespace Globals

